library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity approx_poly is
	generic (
		N		: integer := 10
	);
	port (
		clk 		: in std_logic;
		rst 		: in std_logic;
		input		: in unsigned(N-1 downto 0);
		output		: out unsigned(14 downto 0)
	);
end entity;

architecture behaviour of approx_poly is
	signal in_delay1: unsigned(N-1 downto 0) := (others => '0');
	signal in_delay2: unsigned(N-1 downto 0) := (others => '0');
	signal in_delay3: unsigned(N-1 downto 0) := (others => '0');
	signal in_delay4: unsigned(N-1 downto 0) := (others => '0');

	signal tmp4		: unsigned(17 downto 0) := (others => '0');
	signal tmp3		:   signed(17 downto 0) := (others => '0');
	signal tmp2		:   signed(25 downto 0) := (others => '0');
	signal tmp1		: unsigned(18 downto 0) := (others => '0');
	signal tmp0		: unsigned(23 downto 0) := (others => '0');
begin
	process (clk, rst) is
		constant k1 : unsigned(16 downto 0) := to_unsigned(57699, 17);
		constant k2 : unsigned(16 downto 0) := to_unsigned(77519, 17);
		constant k3 :   signed(17 downto 0) :=   to_signed(-87523, 18);
		constant k4 :   signed(17 downto 0) :=   to_signed(80813, 18);
		constant k5 :   signed(17 downto 0) :=   to_signed(102901, 18);
		constant k6 : unsigned(10 downto 0)  := to_unsigned(1638, 11);

		constant f1 : natural := 9;
		constant f2 : natural := 14;
		constant f3 : natural := 3;
		constant f4 : natural := 18;
		constant f5 : natural := 5;
		constant f6 : natural := 4;

		variable b4	: unsigned(26 downto 0) := (others => '0');
		variable b3	: unsigned(27 downto 0) := (others => '0');
		variable b2	:   signed(28 downto 0) := (others => '0');
		variable b1	:   signed(36 downto 0) := (others => '0');
		variable b0	: unsigned(28 downto 0) := (others => '0');

	begin
		if (rst = '1') then
			in_delay1 <= (others => '0');
			in_delay2 <= (others => '0');
			in_delay3 <= (others => '0');
			in_delay4 <= (others => '0');

			tmp0 <= (others => '0');
			tmp1 <= (others => '0');
			tmp2 <= (others => '0');
			tmp3 <= (others => '0');
			tmp4 <= (others => '0');

			output <= (others => '0');
		elsif (clk'event and clk = '1') then
			in_delay1	<= input;
			in_delay2	<= in_delay1;
			in_delay3	<= in_delay2;
			in_delay4	<= in_delay3;

			b4			:= k1 * input;
			tmp4		<= b4(b4'LEFT downto f1) + k2;

			b3			:= tmp4 * in_delay1;
			tmp3		<= signed('0' & b3(b3'LEFT downto f2)) + k3;

			b2			:= tmp3 * signed('0' & in_delay2);
			tmp2		<= b2(b2'LEFT downto f3) + k4;

			b1			:= tmp2 * signed('0' & in_delay3);
			tmp1		<= unsigned(b1(b1'LEFT downto f4) + k5);

			b0			:= tmp1 * in_delay4;
			tmp0		<= b0(b0'LEFT downto f5) + k6;

			output		<= tmp0(tmp0'LEFT-3 downto f6+2);
		end if;
	end process;
end architecture;