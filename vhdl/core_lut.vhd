library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity core_lut is
	port (
		clk 			: in	std_logic;
		rst 			: in	std_logic;
		sin_out 		: out	signed(15 downto 0);
		counter_out		: out	unsigned(11 downto 0)
	);
end entity;

architecture arch of core_lut is
	COMPONENT counter
		port (
			clk 		: in	std_logic;
			rst			: in	std_logic;
			output		: out	unsigned(11 downto 0)
		);
	END COMPONENT;

	COMPONENT approx_lut
		port (
			clk 		: in 	std_logic;
			rst			: in	std_logic;
			input		: in 	unsigned(7 downto 0);
			output		: out	unsigned(16 downto 0)
		);
	END COMPONENT;

	signal counter_val : unsigned(11 downto 0) := (others => '0');
	signal delay_1	: unsigned(11 downto 0) := (others => '0');
	signal delay_2	: unsigned(11 downto 0) := (others => '0');
	signal delay_3	: unsigned(11 downto 0) := (others => '0');
	signal delay_4	: unsigned(11 downto 0) := (others => '0');
	signal delay_5	: unsigned(11 downto 0) := (others => '0');
	signal delay_6	: unsigned(11 downto 0) := (others => '0');

	signal approx_in	: unsigned(7 downto 0) := (others => '0');
	signal approx_out	: unsigned(16 downto 0) := (others => '0');

	signal nxt			: signed(17 downto 0) :=  18X"00192";
	signal last		: signed(17 downto 0) := -18X"00192"; 
	signal current	: signed(17 downto 0) :=  18X"00192";
	signal diff		: signed(17 downto 0) := (others => '0');

begin
	counter_inst : counter 
		port map (
			clk => clk,
			rst => rst,
			output => counter_val
		);

	approx_inst : approx_lut 
		port map (
			clk => clk,
			rst => rst,
			input => approx_in,
			output => approx_out
		);


	process (clk) is
		variable tmp		: signed(17 downto 0);
	begin
		if rising_edge(clk) then
			if counter_val(10) = '1' then
				-- left
				approx_in <= (255 - counter_val(9 downto 2));
			else
				-- right
				approx_in <= counter_val(9 downto 2);
			end if;

			if delay_1(11) = '1' then
				-- bottom
				nxt <= -signed('0' & approx_out);
			else
				-- top
				nxt <=  signed('0' & approx_out);
			end if;

			if delay_2(1 downto 0) = "00" then
				last <= current;
				current <= nxt;
				diff <= nxt - current;
			else
				diff <= nxt - last;
			end if;
	
			tmp := last + diff(13 downto 2) * signed('0' & delay_3(1 downto 0));
			sin_out <= tmp(17 downto 2);

			delay_1 <= counter_val;
			delay_2 <= delay_1;
			delay_3 <= delay_2;
			delay_4 <= delay_3;
			delay_5 <= delay_4;
			delay_6 <= delay_5;
			
			counter_out <= delay_6;
		end if;
	end process;
end architecture;