library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter is
generic (
	N		: integer := 12
);
port (
	clk 	: in	std_logic;
	rst		: in	std_logic;
	output	: out	unsigned(N-1 downto 0) := (others => '0')
);
end entity;

architecture behaviour of counter is
	signal tmp : unsigned(N-1 downto 0) := (others => '0');
begin
	process (clk, rst) is
	begin
		if (rst = '1') then
			tmp <= (others => '0');
		elsif (clk'event and clk = '1') then
			tmp <= tmp + 1;
		end if;
	end process;
	output <= tmp;
end architecture;