library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity core_poly is
	port (
		clk 			: in	std_logic;
		rst 			: in	std_logic;
		sin_out 		: out	signed(15 downto 0);
		counter_out		: out	unsigned(11 downto 0)
	);
end entity;

architecture arch of core_poly is
	COMPONENT counter
		port (
			clk 		: in	std_logic;
			rst			: in	std_logic;
			output		: out	unsigned(11 downto 0)
		);
	END COMPONENT;

	COMPONENT approx_poly
		port (
			clk 		: in 	std_logic;
			rst			: in	std_logic;
			input		: in 	unsigned(9 downto 0);
			output		: out	unsigned(14 downto 0)
		);
	END COMPONENT;

	signal counter_val : unsigned(11 downto 0) := (others => '0');
	signal delay_1	: unsigned(11 downto 0) := (others => '0');
	signal delay_2	: unsigned(11 downto 0) := (others => '0');
	signal delay_3	: unsigned(11 downto 0) := (others => '0');
	signal delay_4	: unsigned(11 downto 0) := (others => '0');
	signal delay_5	: unsigned(11 downto 0) := (others => '0');
	signal delay_6	: unsigned(11 downto 0) := (others => '0');
	signal delay_7	: unsigned(11 downto 0) := (others => '0');

	signal approx_in		: unsigned(9 downto 0) := (others => '0');
	signal approx_out		: unsigned(14 downto 0) := (others => '0');
begin
	counter_inst : counter 
		port map (
			clk => clk,
			rst => rst,
			output => counter_val
		);

	approx_inst : approx_poly 
		port map (
			clk => clk,
			rst => rst,
			input => approx_in,
			output => approx_out
		);


	process (clk) is
	begin
		if rising_edge(clk) then
			if counter_val(10) = '1' then
				-- left
				approx_in <= (1023 - counter_val(9 downto 0));
			else
				-- right
				approx_in <= counter_val(9 downto 0);
			end if;

			if delay_7(11) = '1' then
				-- bottom
				sin_out <= -signed('0' & approx_out);
			else
				-- top
				sin_out <=  signed('0' & approx_out);
			end if;

			delay_1 <= counter_val;
			delay_2 <= delay_1;
			delay_3 <= delay_2;
			delay_4 <= delay_3;
			delay_5 <= delay_4;
			delay_6 <= delay_5;
			delay_7 <= delay_6;
			
			counter_out <= delay_7;
		end if;
	end process;
end architecture;
