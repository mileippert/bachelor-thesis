library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

entity testbench is
end entity;

architecture behaviour of testbench is
	SIGNAL clk:		std_logic := '0';
	SIGNAL rst:		std_logic := '1';
	CONSTANT tpd:	time := 1 ns;

	COMPONENT core_lut
		port (
			clk 		: in	std_logic;
			rst			: in	std_logic;
			sin_out		: out	signed(15 downto 0);
			counter_out	: out	unsigned(11 downto 0)
		);
	END COMPONENT;

	signal sin_out		: signed(15 downto 0) := (others => '0');

	signal counter		: unsigned(11 downto 0) := (others => '0');
	signal sin_ref_in	: integer := 0;
	signal sin_gen		: integer := 0;
	signal sin_ref		: integer := 0;
	signal sin_dif		: integer := 0;

begin
	clk <= '1' AFTER tpd/2 WHEN clk='0' ELSE '0' AFTER tpd/2;
	rst <= '1', '0' after tpd;

	sin_dif <= sin_ref - sin_gen;

	core_inst : core_lut
		port map (
			clk => clk,
			rst => rst,
			sin_out => sin_out,
			counter_out => counter
		);

	process (clk) is
		variable tmp: real := 0.0;
		variable offset: real := real(1.0/2048.0);
	begin
		if rising_edge(clk) then
			tmp := sin(((real(to_integer(counter)) / 2048.0) + offset) * MATH_PI);
			sin_ref <= integer(tmp * 32767.0);
			sin_gen <= to_integer(sin_out);
		end if;
	end process;
end architecture;
