# Analyse digitaler Sinus-Generatoren und eine effiziente Implementierung in einem FPGA #

Dieses Repository enthält Matlab-Skripte und VHDL-Code, welcher im Rahmen meiner Bachelorarbeit an der Hochschule Konstanz geschrieben wurde.

This repository contains Matlab scripts and VHDL code which were written as part of my bachelor's thesis at Constance University Of Applied Sciences.