x = linspace(0, 2*pi, 1000);

%% x-values for each quadrant
x1 = x(   1 : 250  );
x2 = x( 251 : 500  );
x3 = x( 501 : 750  );
x4 = x( 751 : 1000 );

%% y-values for each sin quadrant
sin1 = sin(x1);
sin2 = sin(x2);
sin3 = sin(x3);
sin4 = sin(x4);

%% x-values for unit-circle
ax1 = cos(x1);
ax2 = cos(x2);
ax3 = cos(x3);
ax4 = cos(x4);

%% y-values for unit-circle
ay1 = sin(x1);
ay2 = sin(x2);
ay3 = sin(x3);
ay4 = sin(x4);

sine_wave_plot(x1/pi,sin1,x2/pi,sin2,x3/pi,sin3,x4/pi,sin4);
unit_circle_plot(ax1,ay1,ax2,ay2,ax3,ay3,ax4,ay4);

