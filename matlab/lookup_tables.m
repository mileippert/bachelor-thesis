% prepare look up tables
lut_table_size = 254;
[lin_lut_x, lin_lut_y] = gen_linear_lut(lut_table_size);
[exp_lut_x, exp_lut_y] = gen_exp_lut(lut_table_size);

lin_lut_x_plot = lin_lut_x;
lin_lut_y_plot = lin_lut_y;

exp_lut_x_plot = exp_lut_x;
exp_lut_y_plot = exp_lut_y;

%% Plot Look-Up Tables
figure('name', 'Lookup Tabelle', 'NumberTitle', 'off');
plot(lin_lut_x_plot, lin_lut_y_plot, exp_lut_x_plot, exp_lut_y_plot);
legend('linear', 'exponential');

%% Plot Result Using Look-Up Tables Without Interpolation
result_llut = zeros(1, plot_x_resolution);
result_elut = zeros(1, plot_x_resolution);

for i = 1:plot_x_resolution
    result_llut(1,i) = lut_lookup(x(1,i), lin_lut_x, lin_lut_y, 0);
    result_elut(1,i) = lut_lookup(x(1,i), exp_lut_x, exp_lut_y, 0);    
end
result_lerr = result_llut - sin_reference;
result_eerr = result_elut - sin_reference;

plotnice('Res L!Lin', x, vertcat(result_llut, result_elut, sin_reference), {'linear'; 'exponential'; 'reference'});
plotnice('Err L!Lin', x, vertcat(result_lerr, result_eerr), {'linear'; 'exponential'}, 'northeast');
plotnice('Res L!Lin', x, vertcat(result_llut, sin_reference), {'linear'; 'reference'});
plotnice('Err L!Lin', x, vertcat(result_lerr), {'linear'}, 'northeast');

%% Plot Result Using Look-Up Tables With Interpolation
result_llut_lin = zeros(1, plot_x_resolution);
result_elut_lin = zeros(1, plot_x_resolution);

for i = 1:plot_x_resolution
    [y_start, x_start] = lut_lookup(x(1,i), lin_lut_x, lin_lut_y, -1);
    [y_end, x_end] = lut_lookup(x(1,i), lin_lut_x, lin_lut_y, 1);
    
    x_diff = x_end - x_start;
    y_diff = y_end - y_start;
    if (x_diff == 0)
        result_llut_lin(1,i) = y_start;
    else
        result_llut_lin(1,i) = y_start + ((y_diff / x_diff) * (x(1,i) - x_start)); 
    end
    
    [y_start, x_start] = lut_lookup(x(1,i), exp_lut_x, exp_lut_y, -1);
    [y_end, x_end] = lut_lookup(x(1,i), exp_lut_x, exp_lut_y, 1);
    
    x_diff = x_end - x_start;
    y_diff = y_end - y_start;
    
    if (x_diff == 0)
        result_elut_lin(1,i) = y_start; 
    else
        result_elut_lin(1,i) = y_start + ((y_diff / x_diff) * (x(1,i) - x_start)); 
    end
end
result_lerr_lin = result_llut_lin - sin_reference;
result_eerr_lin = result_elut_lin - sin_reference;

plotnice('Res LLin', x, vertcat(result_llut_lin, result_elut_lin, sin_reference), {'linear'; 'exponential'; 'reference'});
plotnice('Err LLin', x, vertcat(result_lerr_lin, result_eerr_lin), {'linear'; 'exponential'}, 'northeast');
plotnice('Res LLin', x, vertcat(result_llut_lin, sin_reference), {'linear'; 'reference'});
plotnice('Err LLin', x, vertcat(result_lerr_lin), {'linear'}, 'northeast');

%% Plot Result Using Look-Up Tables With Interpolation And Quantization
table_size = 6;
interpolation_steps = 16;
result_size = table_size * interpolation_steps - (interpolation_steps - 1);

result_quantification = int16(zeros(1, result_size));

[~, y_lookup] = gen_linear_lut(table_size-2);
y_lookup = horzcat(0, y_lookup, 1);

x_impl = uint8(linspace(0, result_size - 1, result_size));
y_lookup = int16(y_lookup * ((2^15)-1));

for i = 0:result_size-1
    lower = y_lookup(1,floor(i/interpolation_steps)+1);
    upper = y_lookup(1,ceil(i/interpolation_steps)+1);
    
    diff = upper - lower;
    diff = bitshift(diff, -log2(interpolation_steps));
    
    if (diff == 0)
        result_quantification(1,i+1) = lower;
        step = 1;
    else
        result_quantification(1,i+1) = lower + diff * step;
        step = step + 1;
    end
end

x_ref = linspace(0, 0.5, result_size*10);
y_ref = int16(sin(x_ref * pi) * ((2^15)-1));

% extend matrices to correct length
% http://de.mathworks.com/matlabcentral/answers/131660
result_quantification = reshape(repmat(result_quantification,10,1),1,[]);
result_quantification_err = result_quantification - y_ref;

plotnice('Res LLin Quant', x_ref, vertcat(result_quantification, y_ref), {'result'; 'reference'});
plotnice('Err LLin Quant', x_ref, vertcat(result_quantification_err), {'error'}, 'northeast');
