%% Approximate Sinus with Polynom
p = [2.7 -6.22 0.1683 3.133 0]; % first point weighted

tmp3 = p(2) + p(1) * x;
tmp2 = p(3) + tmp3 .* x;
tmp1 = p(4) + tmp2 .* x;
result = p(5) + x .* tmp1;

error = result - sin_reference;
plotnice('Result Polynom',x,vertcat(result,sin_reference),{'polynom';'reference'});
plotnice('Error Polynom',x,error,{'error'},'southwest');

% determining quality of approximation
trapz(abs(error))
