%% Generate LUT for 15-Bit and restore Curve from LUT
plot_x_resolution = 1024;
lut_table_size = plot_x_resolution/4;
interpolation_steps = 4;

x_int = int32(linspace(0, plot_x_resolution - 1, plot_x_resolution));
y_int = linspace(0 + 0.25/lut_table_size, .5 - 0.25/lut_table_size, lut_table_size);
y_int = int32(sin(y_int * pi) * ((2^15)-1));

x_ref = linspace(0, .5-0.125/lut_table_size, plot_x_resolution);
y_ref = sin(x_ref * pi) * ((2^15)-1);

result = zeros(1,plot_x_resolution);

last = int32(-y_int(1));
current = int32(y_int(1));
for i = 0:plot_x_resolution-1
    step = mod(i+2,interpolation_steps);
    x_tmp = min(fix((i+2)/interpolation_steps+1), 256);
    next = y_int(x_tmp);
    if current ~= next
        last = current;
        current = next;
    end
    diff = (next - last)/interpolation_steps;
    
    result(1,i+1) = last + diff * step;
end

% create diagram of result
plotnice('Result', x_ref, vertcat(fix(result), fix(y_ref)), {'result'; 'reference'}, 'northeast', true);

% create diagram with raw values
error = result - y_ref;
plotnice('Error',double(x_int)/2048,error,{'error'},'southwest', true);

% create diagram with integer values
error = fix(result) - fix(y_ref);
plotnice('Error',double(x_int)/2048,error,{'error'},'southwest', true);


%% Generate LUT for 17-Bit and restore Curve from LUT
plot_x_resolution = 1024;
lut_table_size = plot_x_resolution/4;
interpolation_steps = 4;

x_int = int32(linspace(0, plot_x_resolution - 1, plot_x_resolution));
y_int = linspace(0 + 0.25/lut_table_size, .5 - 0.25/lut_table_size, lut_table_size);
y_int = int32(sin(y_int * pi) * ((2^17)-4));

x_ref = linspace(0, .5-0.125/lut_table_size, plot_x_resolution);
y_ref = sin(x_ref * pi) * ((2^15)-1);

result = zeros(1,plot_x_resolution);

last = int32(-y_int(1));
current = int32(y_int(1));
for i = 0:plot_x_resolution-1
    step = mod(i+2,interpolation_steps);
    x_tmp = min(fix((i+2)/interpolation_steps+1), 256);
    next = y_int(x_tmp);
    if current ~= next
        last = current;
        current = next;
    end
    diff = (next - last)/interpolation_steps;
    
    result(1,i+1) = last + diff * step;
end

% create diagram of result
plotnice('Result', x_ref, vertcat(fix(result/4), fix(y_ref)), {'result'; 'reference'}, 'northeast', true);

% create diagram with raw values
error = result/4 - y_ref;
plotnice('Error',double(x_int)/2048,error,{'error'},'southwest', true);

% create diagram with integer values
error = fix(result/4) - fix(y_ref);
plotnice('Error',double(x_int)/2048,error,{'error'},'southwest', true);