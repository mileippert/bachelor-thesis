%% Approximate Sinus with Polynom (Real)
if (false)
p = [1.761 0.5893 -5.343 0.02318 3.14043 1.462e-5]; % first point weighted

tmp4 = p(2) + p(1) * x;
tmp3 = p(3) + tmp4 .* x;
tmp2 = p(4) + tmp3 .* x;
tmp1 = p(5) + tmp2 .* x;
result = p(6) + x .* tmp1;

error = result - sin_reference;
plotnice('Result Polynom',x,vertcat(result,sin_reference),{'polynom';'reference'});
plotnice('Error Polynom',x,error,{'error'},'southwest');

% determining quality of approximation
trapz(abs(error))

%% Determinate Koefficients using Command Line
plot_x_resolution = 1024;
x_int = linspace(0, plot_x_resolution - 1, plot_x_resolution);
sin_reference_int = sin(x_int/2048 * pi)* ((2^15)-1);

ft = fittype('poly5');
options = fitoptions(ft);
options.Upper = [+Inf +Inf +Inf +Inf +Inf 0];
options.Lower = [-Inf -Inf -Inf -Inf -Inf 0];
[res, gof, med] = fit(double(x_int'),sin_reference_int',ft,options);
residuals = med.residuals;

%% Approximate Sinus with Polynom (Full Range)
plot_x_resolution = 1024;
x_int = linspace(0, plot_x_resolution - 1, plot_x_resolution);
sin_reference_int = sin(x_int/2048 * pi)* ((2^15)-1);

%p = coeffvalues(res);
%p = [1.634922e-12 1.004571e-09 -2.028556e-05 1.380525e-04 50.252965 0]; % original values
p = [1.6347e-12 1.004571e-09 -2.028556e-05 1.380525e-04 50.252965 0]; % optimized < 0.5 LSB

tmp4 = p(2) + p(1) * x_int;
tmp3 = p(3) + tmp4 .* x_int;
tmp2 = p(4) + tmp3 .* x_int;
tmp1 = p(5) + tmp2 .* x_int;
result = p(6) + x_int .* tmp1;

error = result - sin_reference_int;
plotnice('Result Polynom',x_int,vertcat(result,sin_reference_int),{'polynom';'reference'}, 'northwest', true);
plotnice('Error Polynom',x_int/2048,error,{'Abweichung'},'northwest', true);

%% Approximate Sinus with Polynom (Full Range,Int)
plot_x_resolution = 1024;
x_int = int32(linspace(0, plot_x_resolution - 1, plot_x_resolution));
sin_reference_int = sin(double(x_int)/2048 * pi) * ((2^15)-1);

% exponenten
f = [ 10 14 4 17 9 ];

% koeffizienten
%p = coeffvalues(res);
%p = [1.6347e-12 1.004572e-09 -2.028557e-05 1.380526e-04 50.252965 0]; % original from last step
p = [1.6341e-12 1.004572e-09 -2.028557e-05 1.387e-04 50.25292 0]; % optimized for int

% berechnung koeffizienten f�r implementierung
p = int32([[[[[p(1)*2^f(1) p(2)]*2^f(2) p(3)]*2^f(3) p(4)]*2^f(4) p(5)]*2^f(5) p(6)]);
%          |   ####                                                   |
%          |  ##################                                      |
%          | ################################                         |
%          |##############################################            |
%          ############################################################

% berechnung resultat
tmp4 = p(2) + (p(1) * x_int)    /2^f(1);
tmp3 = p(3) + (tmp4 .* x_int)   /2^f(2);
tmp2 = p(4) + (tmp3 .* x_int)   /2^f(3);
tmp2 = tmp2 / 2^4; % shiften da sonst overflow
tmp1 = p(5) + (tmp2 .* x_int)   /2^(f(4) - 4); % shiften im vorh. schritt beachten!
result = p(6) + (tmp1 .* x_int) /2^f(5);

error = result - int32(sin_reference_int);
plotnice('Result Polynom',double(x_int)/2048,vertcat(result,sin_reference_int),{'polynom';'reference'}, 'northwest', true);
plotnice('Error Polynom',double(x_int)/2048,error,{'error'},'southwest', true);
end

%% Approximate Sinus with Polynom (17bit,int)
plot_x_resolution = 1024;

x_int = int32(linspace(0, plot_x_resolution - 1, plot_x_resolution));
x_ref = linspace(0 + 0.25/plot_x_resolution, .5 - 0.25/plot_x_resolution, plot_x_resolution);
sin_reference_int = sin(x_ref * pi) * ((2^17)-4);

ft = fittype('poly5');
options = fitoptions(ft);
options.Upper = [+Inf +Inf +Inf +Inf +Inf +Inf];
options.Lower = [-Inf -Inf -Inf -Inf -Inf -Inf];
options.Weights = [];
[res, gof, med] = fit(double(x_int'),sin_reference_int',ft,options);
residuals = med.residuals;
fitted_coeff = coeffvalues(res);

% exponenten
f = [ 9 14 3 18 5 4 ];

% koeffizienten
%p = fitted_coeff;
% pre shift
%p = [6.410811642781383e-12 4.378029998721587e-09 -8.151045919414149e-05 7.196994572970047e-04 2.009797206681395e+02 1.879897388497754];
%p = [6.4090e-12 4.3781e-09 -8.1510e-05 7.1969e-04 2.0098e+02 2.00];

% post shift
%p = [6.405900263509484e-12 4.406542412939445e-09 -8.151321209370025e-05 6.021116008889297e-04 2.009795928684764e+02 1.024094130078753e+02];
p = [6.4059e-12 4.4065e-09 -8.1513e-05 6.0211e-04 200.9795 102.4094];

% berechnung koeffizienten f�r implementierung
p = int32(fix([[[[[p(1)*2^f(1) p(2)]*2^f(2) p(3)]*2^f(3) p(4)]*2^f(4) p(5)]*2^f(5) p(6)]*2^f(6)));
bits = ceil(log2(double(abs(p))));
% berechnung resultat
b4 = p(1) * x_int;
tmp4 = b4 / 2^f(1) + p(2);

b3 = tmp4 .* x_int;
tmp3 = b3 / 2^f(2) + p(3);

b2 = tmp3 .* x_int;
tmp2 = b2 / 2^f(3) + p(4);

b1 = int64(tmp2) .* int64(x_int); % zus�tzlicher shift
tmp1 = int32(b1 / 2^f(4)) + p(5); % shiften im vorh. schritt beachten!

b0 = tmp1 .* x_int;
tmp0 = b0 / 2^f(5) + p(6);

result = tmp0 / 2^f(6);

plotnice('Result Polynom',double(x_int)/2048,vertcat(fix(double(result)/4),fix(sin_reference_int/4)),{'polynom';'reference'}, 'northwest', true);
%sin_reference_int = int32(sin(double(x_int)/2048 * pi) * ((2^15)-1));
sin_reference_int = int32(sin(x_ref * pi) * ((2^15)-1));

% create diagram with raw values
error = result - bitshift(sin_reference_int, 2);
plotnice('Error Polynom',double(x_int)/2048,error,{'error'},'southwest', true);

% create diagram with raw values
error = bitshift(result, -2) - sin_reference_int;
plotnice('Error Polynom',double(x_int)/2048,error,{'error'},'southwest', true);
