%% Approximate Sinus with simple Polynom
% manual calculation of parameters. (simply parabola shifted, scaled and inverted)
result_approx_x2 = -4 * (x - 0.5).^2 + 1;
result_approx_x2_err = result_approx_x2 - sin_reference;

plotnice('1:Res', x,vertcat(result_approx_x2,sin_reference),{'x�';'reference'});
plotnice('1:Err', x,result_approx_x2_err,{'error'});

%% Approximate (Co)Sinus with simple Polynom
% manual calculation of parameters. (simply parabola shifted, scaled and inverted)
result_approx_x2 = -4 * x.^2 + 1;
result_approx_x2_err = result_approx_x2 - cos_reference;
result_full_x2 = horzcat(fliplr(result_approx_x2), result_approx_x2, -fliplr(result_approx_x2), -result_approx_x2);

plotnice('2:Res', x,vertcat(result_approx_x2,cos_reference),{'x�';'reference'},'southwest');
plotnice('2:Err', x,result_approx_x2_err,{'error'},'southwest');
plotnice('2:Fll', horzcat(x,0.5+x,1+x,1.5+x),vertcat(result_full_x2, sin_reference_full),{'full reference sinus';'full approximated sinus'},'southwest');

%% Approximate (Co)Sinus with more complex Polynom (Weighted Approximation)
% first and last points are weighted while running appoximation.
% this forces the resulting polynom through points (0/0) and (0.5/1)
result_approx_x2 = fliplr(-3.289 * x.^2 - 0.3556 * x + 1);
result_approx_x2_err = result_approx_x2 - sin_reference;
result_full_x2 = horzcat(result_approx_x2, fliplr(result_approx_x2), -result_approx_x2, -fliplr(result_approx_x2));

plotnice('3:Res', x,vertcat(result_approx_x2,sin_reference),{'x�';'reference'},'southwest');
plotnice('3:Err', x,result_approx_x2_err,{'error'},'southwest');
plotnice('3:Fll', horzcat(x,0.5+x,1+x,1.5+x),vertcat(sin_reference_full, result_full_x2),{'full approximated sinus';'full reference sinus'},'southwest');
