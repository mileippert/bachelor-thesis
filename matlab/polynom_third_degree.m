%% Approximate Sinus with Polynom
p = [-3.683 -0.5578 3.193 0]; % first point weighted
%p = [-3.506 -0.6591 3.206 0]; % both points weighted
%p = [-3.347 -0.8071 3.246 0]; % last points weighted

tmp2 = p(2) + p(1) * x;
tmp1 = p(3) + tmp2 .* x;
result = p(4) + x .* tmp1;

error = result - sin_reference;
plotnice('Result Polynom',x,vertcat(result,sin_reference),{'polynom';'reference'});
plotnice('Error Polynom',x,error,{'error'},'southwest');

squares_due_to_error = error .^ 2;
sum(squares_due_to_error)

restored = horzcat(...
    result, fliplr(result), -result, -fliplr(result), ...
    result, fliplr(result), -result, -fliplr(result), ...
    result, fliplr(result), -result, -fliplr(result), ...
    result, fliplr(result), -result, -fliplr(result) ...
    );
restored_fft = fft(restored);
P2 = abs(restored_fft/plot_x_resolution);
P1 = P2(1:plot_x_resolution/2+1);
P1(2:end-1) = 2*P1(2:end-1);

% determining quality of approximation
trapz(abs(error))

%% Approximate Sinus with Polynom (Values from Implementation )
if (false)
result = zeros(1, plot_x_resolution);

intermediate_x1 = zeros(1, plot_x_resolution);
intermediate_x2 = zeros(1, plot_x_resolution);
intermediate_x3 = zeros(1, plot_x_resolution);
intermediate_input_max = zeros(1, plot_x_resolution);

x_impl = x * 512;
y_impl = sin_reference * (2^16-1);

% p1 = 0.001743 * 16384;
% p2 = 0.1741 * 16384;
% p3 = 414.42 * 256;
p1 = 0.001743 * 16384;
p2 = 0.1741 * 16384;
p3 = 414.42 * 256;

for i = 1:plot_x_resolution
    tmp2 = p2 + p1 * x_impl(1,i);
    tmp1 = p3 - tmp2 * x_impl(1,i) / (16384 / 256);
    result(1,i) = x_impl(1,i) * tmp1 / 256;
 
    intermediate_x1(1,i) = tmp2;
    intermediate_x2(1,i) = tmp1;
    intermediate_x3(1,i) = result(1,i);
    intermediate_input_max(1,i) = (2^17) - 1;
end
error = result - y_impl;
result = result / 65535;
y_impl = y_impl / 65535;
error = error / 65535;

plotnice('Result Polynom',x,vertcat(result,y_impl),{'polynom';'reference'});
plotnice('Error Polynom',x,error,{'error'},'southwest');
end

%% Approximate Sinus with Polynom (floor values to analyse influence of quantification)
if (false)
result = uint64(zeros(1, plot_x_resolution));

intermediate_x1 = uint64(zeros(1, plot_x_resolution));
intermediate_x2 = uint64(zeros(1, plot_x_resolution));
intermediate_x3 = uint64(zeros(1, plot_x_resolution));
intermediate_input_max = uint64(zeros(1, plot_x_resolution));

x_impl = floor(x * 512);
y_impl_sin = sin_reference * (2^16-1);
y_impl_cos = cos_reference * (2^16-1);

% p1 = uint64( 0.001743 * 16384 );
% p2 = uint64( 0.1741 * 16384 );
% p3 = uint64( 414.42 * 256 );
p1 = uint64( 0.001743 * 16384 );
p2 = uint64( 0.1741 * 16384 );
p3 = uint64( 414.42 * 256 );

for i = 1:plot_x_resolution
    tmp2 = p2 + p1 * x_impl(1,i);
    tmp1 = p3 - tmp2 * x_impl(1,i) / (16384 / 256);
    result(1,i) = x_impl(1,i) * tmp1 / 256;
 
    intermediate_x1(1,i) = tmp2;
    intermediate_x2(1,i) = tmp1;
    intermediate_x3(1,i) = result(1,i);
    intermediate_input_max(1,i) = (2^17) - 1;
end
error = double(result) - y_impl;
result = double(result) / 65535;
y_impl = y_impl / 65535;
error = error / 65535;

plotnice('Result Polynom',x,vertcat(result,y_impl),{'polynom';'reference'});
plotnice('Error Polynom',x,error,{'error'},'southwest');
end