%% Approximate Cosinus with Taylor
x_taylor = x * pi;

x2 = x_taylor .* x_taylor;
x4 = x2 .* x2;

result_norm = 1 - x2/2 + x4/24;
result_opti = 1 - x2/2 + (x4/32 + x4/128);

error_norm = result_norm - cos_reference;
error_opti = result_opti - cos_reference;

%
plotnice('Result Polynom',x,vertcat(result_norm,cos_reference),{'taylor';'reference'});
plotnice('Error Polynom',x,vertcat(error_norm),{'error'},'southwest');

%
plotnice('Result Polynom',x,vertcat(result_norm,result_opti,cos_reference),{'with division';'without division';'reference'});
plotnice('Error Polynom',x,vertcat(error_norm,error_opti),{'with division';'without division'},'southwest');

% determining quality of approximation
trapz(abs(error_norm))
