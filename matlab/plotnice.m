function plotnice(figureName, xData, lineData, lineNames, legendLocation, stairs_plot)
if (exist('legendLocation','var') ~= 1)
    legendLocation = 'southeast';
end
if (exist('stairs_plot','var') ~= 1)
    stairs_plot = false;
end
% lineNames = {'first';'second';'third'}
figure1 = figure('NumberTitle','off','Name',figureName);
axes1 = axes('Parent',figure1,'Position',[0.10 0.10 0.5 0.5]);
hold(axes1,'on');

% Create multiple lines using matrix input to plot
if (stairs_plot)
    plot1 = stairs(xData',lineData');
else
    plot1 = plot(xData,lineData);
end
set(plot1, {'DisplayName'}, lineNames);

box(axes1,'on');
set(axes1,'PlotBoxAspectRatio',[1 1 1],'XGrid','on','YGrid','on');
set(axes1,'OuterPosition',[0.05 0.05 0.9 0.9]);
legend1 = legend(axes1,'show');
set(legend1,'Location', legendLocation);
end