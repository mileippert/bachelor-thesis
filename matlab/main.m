%% preparation
plot_x_resolution = 8192;
x = linspace(0, plot_x_resolution - 1, plot_x_resolution);
x = x / ((plot_x_resolution - 1) * 2);

% for reference
sin_reference = sin(x * pi);
cos_reference = cos(x * pi);
sin_reference_full = horzcat( ...
     sin_reference, ...
     fliplr(sin_reference), ...
    -sin_reference, ...
    -fliplr(sin_reference) ...
);

% for approximation
approx_weights_first = ones(1, plot_x_resolution);
approx_weights_both = ones(1, plot_x_resolution);
approx_weights_last = ones(1, plot_x_resolution);
approx_weights_first(1) = 1e20;
approx_weights_last(plot_x_resolution) = 1e20;
approx_weights_both(1) = 1e20;
approx_weights_both(plot_x_resolution) = 1e20;