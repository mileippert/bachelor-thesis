function [ x_indizes, y_values ] = gen_exp_lut( size )
% This function will return two arrays:
%   the first one contains the time in pi distributed between 0 and pi/2
%   the second contains the value at the time index
    disp('generating exponential lookup table');
    
    initial_stepsize = 0.25;
    x_indizes = zeros(1,size);
    x_indizes(1,1) = initial_stepsize;
    for i = 2:size
        x_indizes(1,i) = x_indizes(1, i - 1) + initial_stepsize / 2^(i-1);
    end
    y_values = sin(x_indizes * pi);
end

