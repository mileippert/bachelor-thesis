function [ y_value, x_value ] = lut_lookup( x_value_in, lut_x, lut_y, mode )
% mode options:
% -1 -> floor
%  0 -> round
%  1 -> ceiling
lut_table_size = size(lut_x,2);

if mode == -1
    % fallback auf ersten wert
    x_value = 0;
    y_value = 0;
    % vorne anfangen
    index = 1;

    while x_value_in >= lut_x(1,index)
        x_value = lut_x(1,index);
        y_value = lut_y(1,index);
        
        if index + 1 > lut_table_size
            break
        end
        index = index + 1;
    end
elseif mode == 1
    % fallback auf letzen wert
    x_value = 0.5;
    y_value = 1;
    % hinten anfangen
    index = lut_table_size;
    
    while x_value_in <= lut_x(1,index)
        
        x_value = lut_x(1,index);
        y_value = lut_y(1,index);
        
        if index - 1 <= 0
            break
        end
        index = index - 1;
    end
else
    [fy, fx] = lut_lookup(x_value_in, lut_x, lut_y, -1);
    [cy, cx] = lut_lookup(x_value_in, lut_x, lut_y, 1);
   
    if x_value_in >= (fx + (cx - fx) / 2)
        x_value = cx;
        y_value = cy;
    else
        x_value = fx;
        y_value = fy;
    end
end
% display result
disp(x_value)
disp(y_value)

end

