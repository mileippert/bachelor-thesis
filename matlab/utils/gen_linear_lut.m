function [ x_indizes, y_values ] = gen_linear_lut( size )
% This function will return two arrays:
%   the first one contains the time in pi distributed between 0 and pi/2
%   the second contains the value at the time index
    disp('generating linear lookup table');
    x_indizes = zeros(1,size);

    for i = 1:size
        x_indizes(1,i) = ((1000000/2 / (size + 1))) * i / 1000000;
    end
    y_values = sin(x_indizes * pi);
        
%    disp('x-values:');
%    disp(x_indizes);
%    disp('y-values:');
%    disp(y_values);
end

